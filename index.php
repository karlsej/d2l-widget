<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Library Search Widget for D2L</title> 
    <meta name="description" content="">
    <meta name="author" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" >
	<meta name="robots" content="noindex">

    <!-- Le styles -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
  html {background:#fff;}
      body {
        background-color: #eee;
				width:300px;
				height:300px;
				padding:0;
				margin:0 auto;
      }
 .container {width:300px; background-color: transparent;}
input[type='text'], {
        height: auto;
        margin-bottom: 10px;
        padding: 7px 9px;
      }
.tab-content {background:#eee; padding-right:8px;  /* background-image:url(img/MightyDeals_DirtyAntiquePap.jpg); */ border-left:1px solid #ddd; border-right: 1px solid #ddd;border-bottom: 1px solid #ddd; height:280px; overflow:hidden;}
/* .tab-content p {background: rgba(255,255,255,0.8); padding:0 4px;} */
.nav {margin-bottom:0;} 
 .nav-tabs {background:#fff; font-weight:bold; font-size:12px;}  
.nav-tabs .active a {background:#eee;}
.nav-tabs .active a:hover {background:#eee;}
.tab-pane {padding-top:12px; padding-left:12px;}
.row {width:280px;}
.input-xlarge {width:240px !important;}
.radio {font-size:11px; margin-left:2px !important;}
/* #edsblurb {display:none;} */
#catblurb {font-size:11px; font-style:italic; padding:0; margin:-10px 0 10px 0;}
#edsblurb {position:relative; width:160px; bottom:52px; font-size:11px; font-style:italic; margin-bottom:-52px;}
#eds-boxandradio {margin-left:4px;}
#eds-search, #cat-search {margin-top:10px; margin-left:10px;}
#cat-input {margin-left:-8px;}
#eds-submit {margin-left:190px; margin-top:14px; }
#cat-submit {margin-left:180px; margin-top:6px;}
.libraryh3lp {text-align:right; margin-right:6px; float:right;}
#aboutreserveslink {display:none;}
.input-medium {width:100px;}
#reservessearch div.span3 {width:120px;}
#reservessubmit {margin-left:200px;}
.btn {margin-bottom:6px;}
.tab-content ul {text-align:center;}
ul.dropdown-menu {min-width:80px;}
ul.typeahead {text-align:left;}
#reserves-keywords ul.typeahead {max-width:158px;}
#reserves-keywords ul li a {white-space:normal; font-size:12px;}
#catform-reserveslink {display:none;}


    </style>
		
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-15285327-1', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);

</script>
		
  </head>

  <body>
<div class="container"> 
<!-- Tabs -->

<div class="tabbable"><ul class="nav nav-tabs">
  <li class="active"><a href="#onesearchtab" data-toggle="tab">OneSearch</a></li>
  <li ><a href="#catalogtab" data-toggle="tab">Books</a></li>

  <li><a href="#textbooktab" data-toggle="tab" title="Find textbooks and other items on reserve">Textbooks</a></li>
</ul>
             <div class="tab-content">
<!-- OneSearch Tab	-->					 
<div class="tab-pane active" id="onesearchtab">

<form class="form-search" action="https://wserver.scc.losrios.edu/~library/tools/newsearch/process/eds-processform.php" method="post" id="eds-search" target="_blank">
<div class="row">
<div class="span4" id="eds-boxandradio">
<input type="text" class="input-xlarge search-query" id="la_qentry" name="query" placeholder="Books, ebooks, articles, DVDs &amp; more..."  autocomplete="off">

<label class="radio inline">
  <input type="radio" name="edsScope" value="eds-All" checked>
  Everything
</label>

<label class="radio inline">
  <input type="radio" name="edsScope" value="eds-Ebooks">
  Ebooks  
</label>
<label class="radio inline">
  <input type="radio" name="edsScope" value="eds-Articles">
  Scholarly articles  
</label> 
</div>
<div >
<input type="hidden" value="d2l" name="onesearch-source" >
<input type="submit" value="Search" id="eds-submit" class="btn btn-primary">
</div>

</div>
</form>

<p id="edsblurb">OneSearch provides access to almost all library resources. <a href="http://researchguides.scc.losrios.edu/about-onesearch" target="_blank"><i class="icon-info-sign"></i>Read more.</a> <span id="textbook-note"><strong>For textbooks, use <a href="http://www.scc.losrios.edu/library/services/textbooks-on-reserve/" target="_blank">textbook search</a></strong>.</span></p>
<div id="dblinks">
<ul class="inline">
<li><a href="https://www.library.losrios.edu/resources/databases/?college=scc" onclick="ga('send', 'event', 'links', 'click', 'Databases page');" class="btn btn-small" target="_blank">Go to all Research Databases &raquo;</a></li>
<li><a href="http://atoz.ebsco.com/Titles/9823" onclick="ga('send', 'event', 'links', 'click', 'A-to-Z');"  class="btn btn-small" target="_blank">Find a journal by title &raquo;</a></li>
</ul>
</div>
<!-- <a href="#" class="btn btn-link">Feedback</a> -->
										<!-- begin libraryh3lp code -->
										<!-- Place this div in your web page where you want your chat widget to appear. -->
<div class="needs-js"></div>

<div class="libraryh3lp" jid="d2l-widget@chat.libraryh3lp.com" style="display: none;">
  <a href="https://libraryh3lp.com/chat/d2l-widget@chat.libraryh3lp.com?skin=22093&amp;identity=librarian" 
   onclick="window.open('https://libraryh3lp.com/chat/d2l-widget@chat.libraryh3lp.com?skin=22093&amp;identity=librarian','chat', 'resizable=1,width=220,height=420,left=200,top=100'); return false;" class="btn btn-success btn-mini ttip" title="Chat online with a librarian. A new window will open"><i class="icon-signal icon-white"></i> Connect to live chat
  </a>
</div>

<div class="libraryh3lp" style="display: none;">
<a href="http://www.scc.losrios.edu/library/services/ask-librarian/" class="btn btn-mini" target="_blank"><img src="img/libraryh3lpstrip.png" alt="Contact the library" title="Contact the library, in person or online" class="ttip" /></a>
</div>

</div>	
<!-- Catalog Tab -->
<div class="tab-pane" id="catalogtab">

<form class="form-search" action="https://wserver.scc.losrios.edu/~library/tools/newsearch/process/cat-processform.php" id="cat-search" method="post" name="cat-search" target="_blank">
<input type="text" class="input-xlarge search-query" placeholder="keywords, title, subject, author, etc." name="searcharg" id="cat-input">
<input type="hidden" value="d2l" name="cat-source" >
<input type="submit" value="Search" class="btn btn-primary" id="cat-submit">

</form>
<p id="catblurb">Search for books on the shelves of the Los Rios Libraries.<!-- <a href="#"><i class="icon-info-sign"></i> Read more</a> --> <span id="catform-reserveslink"><strong>For textbooks, use <a href="http://www.scc.losrios.edu/library/services/textbooks" target="_blank">textbook search</a></strong>.</span></p>
<div id="catlinks">

<p><a href="http://lois.losrios.edu/patroninfo" onclick="ga('send', 'event', 'links', 'click', 'My Record');" target="_blank" class="btn btn-small">Renew Books/Library Record &raquo;</a></p>

</div>

</div>	

<!-- reserves tab -->				 
						 
                    <div class="tab-pane" id="textbooktab">
		
<form class="form-search-widget" action="https://wserver.scc.losrios.edu/~library/tools/newsearch/process/reserves-processform.php" id="reservessearch" method="post"  name="reservessearch" target="_blank" onsubmit="return formValidator()">
	<div class="row">
<div class="span3">
<label for="Department">Department (e.g. Math):</label><select class="input-medium" id="Department" name="Department"  >
<option selected="selected" value=" ">Select</option>
<option value="ACCT">ACCT - Accounting</option>
<option value="ADMJ">ADMJ - Administration of Justice</option>
<option value="AERO">AERO - Aeronautics</option>
<option value="AH">AH - Allied Health</option>
<option value="ANTH">ANTH - Anthropology</option>
<option value="ARABIC">ARABIC - Arabic</option>
<option value="ART">ART - Art</option>
<option value="ARTH">ARTH - Art History</option>
<option value="ASTR">ASTR - Astronomy</option>
<option value="BIOL">BIOL - Biology</option>
<option value="BUS">BUS - Business</option>
<option value="BUSTEC">BUSTEC - Business Technology</option>
<option value="CANT">CANT - Cantonese</option>
<option value="CHEM">CHEM - Chemistry</option>
<option value="CISA">CISA - Computer Info Science - Apps</option>
<option value="CISC">CISC - Computer Info Science - Core</option>
<option value="CISN">CISN - Computer Info Science - Network</option>
<option value="CISP">CISP - Computer Info Science - Program</option>
<option value="CISS">CISS - Computer Info Science - Security</option>
<option value="CISW">CISW - Computer Info Science - Web</option>
<option value="COMM">COMM - Communication</option>
<option value="COSM">COSM - Cosmetology</option>
<option value="DAST">DAST - Dental Assisting</option>
<option value="DHYG">DHYG -Dental Hygiene</option>
<option value="ECE">ECE - Early Childhood Education</option>
<option value="ECON">ECON - Economics</option>
<option value="ET">ET - Electronics Technology</option>
<option value="ENGR">ENGR - Engineering</option>
<option value="EDT">EDT - Engineering Design Tech</option>
<option value="ENGCW">ENGCW - English Creative Writing</option>
<option value="ENGED">ENGED - English Education</option>
<option value="ENGLB">ENGLB - English Laboratory</option>
<option value="ENGLT">ENGLT - English Literature</option>
<option value="ENGRD">ENGRD - English Reading</option>
<option value="ENGWR">ENGWR - English Writing</option>
<option value="ESL">ESL - English as a Second Language</option>
<option value="ESLG">ESLG - ESL Grammar</option>
<option value="ESLL">ESLL - ESL Listening</option>
<option value="ESLP">ESLP - ESL Pronunciation</option>
<option value="ESLR">ESLR - ESL Reading</option>
<option value="ESLW">ESLW - ESL Writing</option>
<option value="FCS">FCS - Family and Consumer Science</option>
<option value="FASHN">FASHN - Fashion</option>
<option value="FLTEC">FLTEC - Flight Technology</option>
<option value="FREN">FREN - French</option>
<option value="GEOG">GEOG - Geography</option>
<option value="GEOL">GEOL - Geology</option>
<option value="GERON">GERON - Gerontology</option>
<option value="GCOM">GCOM - Graphic Communication</option>
<option value="HEED">HEED - Health Education</option>
<option value="HCD">HCD - Human Career Development</option>
<option value="HIST">HIST - History</option>
<option value="HUM">HUM - Humanities</option>
<option value="HSER">HSER - Human Services</option>
<option value="INDIS">INDIS - Interdisciplinary Studies</option>
<option value="JAPAN">JAPAN - Japanese</option>
<option value="JOUR">JOUR - Journalism</option>
<option value="KINES">KINES - Physical Education - Kinesiology</option>
<option value="KOREAN">KOREAN - Korean</option>
<option value="LIBR">LIBR - Library</option>
<option value="LIBT">LIBT - Library Technology</option>
<option value="LTAT">LTAT - Learn Tutor Acad Tech</option>
<option value="MAND">MAND - Mandarin</option>
<option value="MATH">MATH - Mathematics</option>
<option value="MET">MET - Mechanical Electrical Tech</option>
<option value="MGMT">MGMT - Management</option>
<option value="MKT">MKT - Marketing</option>
<option value="MUFHL">MUFHL - Music Fundmntals, History &amp; Lit</option>
<option value="MUIVI">MUIVI - Instrumental/Voice Instruction</option>
<option value="MUSM">MUSM - Specializations in Music</option>
<option value="NURSE">NURSE - Nursing</option>
<option value="NUTRI">NUTRI - Nutrition and Foods</option>
<option value="OTA">OTA - Occupational Therapy Assistant</option>
<option value="PHIL">PHIL - Philosophy</option>
<option value="PHOTO">PHOTO - Photography</option>
<option value="PACT">PACT - Personal Activities</option>
<option value="PTA">PTA - Physical Therapist Assistant</option>
<option value="PHYS">PHYS - Physics</option>
<option value="POLS">POLS - Political Science</option>
<option value="PSYC">PSYC - Psychology</option>
<option value="RAILR">RAILR - Railroad Operations</option>
<option value="RE">RE - Real Estate</option>
<option value="RECR">RECR - Recreation</option>
<option value="RUSS">RUSS - Russian</option>
<option value="SILA">SILA - Sign Language Studies</option>
<option value="SOCSC">SOCSC - Social Science</option>
<option value="SOC">SOC - Sociology</option>
<option value="SPAN">SPAN - Spanish</option>
<option value="SPORT">SPORT - Sports</option>
<option value="STAT">STAT - Statistics</option>
<option value="TGLG">TGLG - Tagalog</option>
<option value="TA">TA - Theatre Arts</option>
<option value="VIET">VIET - Vietnamese</option>
<option value="VN">VN - Vocational Nursing</option>
<option value="WEXP">WEXP - Work Experience</option>
</select>

</div>
<div class="span3"><label for="ProfessorsLastName" class="ttip">Professor's Last Name:</label> <input id="ProfessorsLastName" name="ProfessorsLastName" placeholder="Last Name only" class="input-medium"  type="text" autocomplete="off" /></div>


 
</div>    
<div class="row">
<div class="span3">
 <label for="CourseNumber" title="Examples: 101, 300, 312. If you're not sure, leave it blank." class="ttip" > Number (e.g. 300): </label> <input id="CourseNumber" name="CourseNumber" placeholder="e.g. 300" class="input-small" type="text"  /></div>


<div class="span3" id="reserves-keywords" ><label for="TitleWords" class="ttip" title="Author's last name or book title, or leave it blank. NO EDITION NUMBERS! No typos!" >Title or author <span class="text-warning"> (optional)</span></label> <input id="TitleWords" name="TitleWords" class="input-medium" type="text" autocomplete="off" />
<input name="reserves-source" type="hidden" value="d2l">

</div>
</div>
<div class="row">
<div class="span3" id="aboutreserveslink"><a class="muted" href="http://www.scc.losrios.edu/library/services/textbooks-on-reserve/" target="_blank"><i class="icon-info-sign"></i> <em>About textbooks on reserve</em></a></div>
<div  id="reservessubmitcontainer"><input  id="reservessubmit"   type="submit" value="Find It" class="btn btn-primary "  /></div>
</div>
</form>
<script type="text/javascript" src="js/validate.js">

</script>
</div>
 
			</div>
	</div>
 </div>

 <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!-- Google-hosted jquery, different from home page because D2L users won't be on limited computers -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="js/html5.js" type="text/javascript"></script>
    <![endif]-->
<!-- <script type="text/javascript">
$('.ttip').tooltip({placement: 'top'})
</script>
 -->

<script src="js/typeahead.js" type="text/javascript">
</script>
<!-- https://github.com/mathiasbynens/jquery-placeholder to show placeholder in IE - but breaks validation in reserves form? can be used in the other forms
<script type="text/javascript" src="js/jquery.placeholder.min.js">
</script>
<script type="text/javascript">

$('input, textarea').placeholder();
 -->
 <!-- Place this script as near to the end of your BODY as possible. -->
 <script>
	// event tracking
$('#eds-search').on('click', function() {
	var q = $('#la_qentry').val();
	var scope = $('input[name=edsScope]:checked', '#eds-search').val();
	ga('send', 'event', 'd2l search widget - eds - ' + scope, 'submit', q);
	});
$('#cat-search').on('click', function() {
	var q = $('#cat-input').val();
	ga('send', 'event', 'd2l search widget - books', 'submit', q);
	});
 </script>
<script  type="text/javascript"  src="https://libraryh3lp.com/js/libraryh3lp.js?multi,poll">
</script>
  </body>
</html>