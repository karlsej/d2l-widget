function formValidator(){
		var coursenumber = document.getElementById('CourseNumber');
		var profname = document.getElementById('ProfessorsLastName');
		if(isCourseNo (coursenumber, "Under 'Number,' enter only numbers.\n\nNOT the 5-digit course code.\n\nUse the number you see next to the department on the class schedule.  \n\nExamples would include 11, 300, or 345.\n\nIf you're not sure, leave it blank.")){
			if(isAlphabet(profname, "Under 'Professor's Last Name,' please only enter the LAST NAME. \n\nDo not include any punctuation or extra spaces.")){
                                 ga('send', 'event', 'd2l search widget - reserves', 'submit', document.getElementById('Department').value + ' ' + coursenumber.value +' ' + profname.value + ' '+ document.getElementById('TitleWords').value);
		return true;
		}
		}
		return false;
	}
	
		
function isCourseNo(elem, helperMsg){
	var numExp = /^[0-9]{2,3}([.][0-9]{1})?$/;
	if(elem.value.match(numExp) || elem.value == ''){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		elem.select();
		return false;
	}
}		


function isAlphabet(elem, helperMsg){
	var alphaExp = /^\s*[a-zA-Z_-]+$/;
	if(elem.value.match(alphaExp) || elem.value == ''){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		elem.select();
		return false;
	}
}